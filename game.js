var player1 = {
    image: "<img src='images/o.png' title='O'>",
    turn: 'O'
},
    player2 = {
        image: "<img src='images/x3.png' title='X'>",
        turn: 'X'
    },
    firstPlayerMove = true;

function nextMove(square) {
    if (square.innerHTML == '') {
        if (firstPlayerMove) {
            square.innerHTML = player1.turn;
        } else
            square.innerHTML = player2.turn;
        firstPlayerMove = !firstPlayerMove;
        winnerAlert(player1.turn);
        winnerAlert(player2.turn);
    }
}

function resetBoard() {
    for (i = 0; i < 10; i++) {
        if (document.getElementById(i) != null) {
            document.getElementById(i).innerText = "";
        }
    }
}
function getBox(number) {
    return document.getElementById(number).innerText;
}



